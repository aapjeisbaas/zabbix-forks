FROM zabbix/zabbix-server-pgsql:ubuntu-6.0-latest
USER root
RUN apt update; apt install locales; localedef -i en_US -c -f utf8 -A /usr/share/locale/locale.alias en_US.UTF-8
USER zabbix